"""
Author: Julia Hu
JHED: jhu26
Name: triangle_count.py

Get the list of cycle triangles in the graph
"""

from pyspark import SparkContext
from time import time
import sys, os

#TODO: Possibly define functions up here
def splitLine(s):
    
    friends = s.strip().split()
    myArray = []

    if len(friends) >= 3:

        for x in range(1, len(friends)-1):
            for y in range(x+1, len(friends)):
                triple = [friends[0], friends[x], friends[y]]
                # make triple in order
                triple.sort(reverse = True)
                myArray.append(triple)

    return myArray
       #how to return all tuples!! 
       #return '%s\t%s' % (triple, 1)

# NOTE: Do not change the name/signature of this function
def count_triangles(data, master="local[2]"):
    """
    @brief: Count triangles using Spark
    @param data: The data location for the input files
    @param master: The master URL as defined at
        https://spark.apache.org/docs/1.1.0/submitting-applications.html#master-urls
    """

    #################  NO EDITS HERE ###################
    assert not os.path.exists("triangles.out"), "File: triangles.out \
            already exists"
    sc = SparkContext(master, "Triangle Count")
    start = time()
    ###############  END NO EDITS HERE  ################
    #makes RDD for you
    lines = sc.textFile(data)
    tuples = lines.flatMap(splitLine) \
                .map(lambda s: (s,1)) \
                .reduceByKey(lambda a, b: a + b)

    output = tuples.collect()
    print output

    sc.stop()

    #################  NO EDITS HERE  ###################
    print "\n\n*****************************************"
    print "\nTotal algorithm time: %.4f sec \n" % (time()-start)
    print "*****************************************\n\n""" 
    ###############  END NO EDITS HERE ################

    with open("triangles.out", "wb") as f:
        for i in range len(output):
            f.write(output[i]) # TODO: Loop with f to write your result to file serially
        #pass


#################  NO EDITS HERE  ###################
if __name__ == "__main__":
    if len(sys.argv) == 2:
        print "Counting triangles with master as 'local[2]'"
        count_triangles(sys.argv[1])
    elif len(sys.argv) == 3: 
        print "Counting triangles with master as '%s'" % sys.argv[2]
        count_triangles(sys.argv[1], sys.argv[2])
    else:
        sys.stderr.write("\nusage: SPARK_ROOT/bin/spark-submit \
            example/python/tri_count.py data_dir [master-url]")
        exit(1)
############### NO EDITS BELOW EITHER ################